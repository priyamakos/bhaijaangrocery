package com.adjit.bhaijan;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.bhaijan.NetworkConnectivity.NetworkConnection;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import Config.BaseURL;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.JSONParser;
import util.JSONParserp;
import util.Session_management;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private static String TAG = LoginActivity.class.getSimpleName();

    private RelativeLayout btn_continue, btn_register;
    private EditText et_password, et_email;
    private TextView tv_password, tv_email, btn_forgot;
    private Session_management sessionManagement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title
        setContentView(R.layout.activity_login);

        et_password = (EditText) findViewById(R.id.et_login_pass);
        et_email = (EditText) findViewById(R.id.et_login_email);
        tv_password = (TextView) findViewById(R.id.tv_login_password);
        tv_email = (TextView) findViewById(R.id.tv_login_email);
        btn_continue = (RelativeLayout) findViewById(R.id.btnContinue);
        btn_register = (RelativeLayout) findViewById(R.id.btnRegister);
       // btn_forgot = (TextView) findViewById(R.id.btnForgot);

        btn_continue.setOnClickListener(this);
        btn_register.setOnClickListener(this);
       // btn_forgot.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnContinue) {
            attemptLogin();
            //paytm_getway();
           // sendUserDetailTOServerdd dl = new sendUserDetailTOServerdd();
            //dl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            // makeRequest();
        // onStartTransaction();
        } else if (id == R.id.btnRegister) {
            Intent startRegister = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(startRegister);
        }
    }

    private void attemptLogin() {

        tv_email.setText(getResources().getString(R.string.et_login_phone_hint));
        tv_password.setText(getResources().getString(R.string.tv_login_password));
        tv_password.setTextColor(getResources().getColor(R.color.black));
        tv_email.setTextColor(getResources().getColor(R.color.black));
        String getpassword = et_password.getText().toString();
        String getemail = et_email.getText().toString();
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(getpassword)) {
            tv_password.setTextColor(getResources().getColor(R.color.black));
            focusView = et_password;
            cancel = true;
        } else if (!isPasswordValid(getpassword)) {
            tv_password.setText(getResources().getString(R.string.password_too_short));
            tv_password.setTextColor(getResources().getColor(R.color.black));
            focusView = et_password;
            cancel = true;
        }

        if (TextUtils.isEmpty(getemail)) {

            tv_email.setTextColor(getResources().getColor(R.color.black));
            focusView = et_email;
            cancel = true;
        }else if (!isPhoneValid(getemail)) {
            tv_email.setText(getResources().getString(R.string.invalide_email_address));
            tv_email.setTextColor(getResources().getColor(R.color.black));
            focusView = et_email;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (focusView != null)
                focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            if (ConnectivityReceiver.isConnected()) {
                makeLoginRequest(getemail, getpassword);
            }
        }

    }

    private boolean isPhoneValid(String getemail) {
                boolean check = false;
            //if(!Pattern.matches("[a-zA-Z]+", phone))
            //{
            if (getemail.length() == 10)// || phone.length() > 0
            {
                //if(phone.length() != 10)
                // {
                check = true;

                Log.d("motest", "false");

                //txtPhone.setError("Not Valid Number");
                // }
            } else {
                Toast.makeText(getApplicationContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
                Log.d("motest", "true");
                check = false;
            }
            //} else
            // {
            //   check=false;
            //}
            return check;

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /*
     * Method to make json object request where json response starts wtih
     */
    private void makeLoginRequest(String email, final String password) {

        // Tag used to cancel the request
        String tag_json_obj = "json_login_req";
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_email", email);
        params.put("password", password);

        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.LOGIN_URL, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                Log.e("loginresponse", response.toString());

                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {
                        JSONObject obj = response.getJSONObject("data");
                        String user_id = obj.getString("user_id");
                        String user_fullname = obj.getString("user_fullname");
                        String user_email = obj.getString("user_email");
                        String user_phone = obj.getString("user_phone");
                        String user_image = obj.getString("user_image");
                        String wallet_ammount = obj.getString("wallet");
                        String reward_points = obj.getString("rewards");
                        Session_management sessionManagement = new Session_management(LoginActivity.this);
                        sessionManagement.createLoginSession(user_id, user_email, user_fullname, user_phone, user_image, wallet_ammount, reward_points, "", "", "", "", password);
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();

                    } else {
                        String error = response.getString("error");
                        Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    //paytm payment getway
    public void paytm_getway() {
         PaytmPGService Service = PaytmPGService.getStagingService();
        //PaytmPGService Service = PaytmPGService.getProductionService();

        HashMap<String, String> paramMap = new HashMap<String,String>();
        paramMap.put( "MID" , "OKkAtf76247387509530");
        // Key in your staging and production MID available in your dashboard
        paramMap.put( "ORDER_ID" , "order1");
        paramMap.put( "CUST_ID" , "cust123");
        paramMap.put( "MOBILE_NO" , "9867777860");
        paramMap.put( "EMAIL" , "username@emailprovider.com");
        paramMap.put( "CHANNEL_ID" , "WAP");
        paramMap.put( "TXN_AMOUNT" , "100.12");
        paramMap.put( "WEBSITE" , "WEBSTAGING");
        // This is the staging value. Production value is available in your dashboard
        paramMap.put( "INDUSTRY_TYPE_ID" , "Retail");
        // This is the staging value. Production value is available in your dashboard
        paramMap.put( "CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=order1");
        paramMap.put( "CHECKSUMHASH" , "tXK82LBKeMiVrR7Kq0eKsj13b/9rA2kJM69RYnN4SNbRG4g2Jo8veMm/YlVyfMDzTEjgc37zA+MOSapeL6bIWkFulVyLzgantsUmdT7Siek=");

        //w2QDRMgp1234567JEAPCIOmNgQvsi+BhpqijfM9KvFfRiPmGSt3Ddzw+oTaGCLneJwxFFq5mqTMwJXdQE2EzK4px2xruDqKZjHupz9yXev4=
        PaytmOrder Order = new PaytmOrder(paramMap);
        Service.initialize(Order,null);
        Service.startPaymentTransaction(this, true, true, new PaytmPaymentTransactionCallback() {
            @Override
            public void onTransactionResponse(Bundle inResponse) {
                Log.d("inResponse",""+inResponse);
            }

            @Override
            public void networkNotAvailable() {
                Log.d("networkNotAvailable","networkNotAvailable");
            }

            @Override
            public void clientAuthenticationFailed(String inErrorMessage) {
                Log.d("clientAuthnFailed",""+inErrorMessage);
            }

            @Override
            public void someUIErrorOccurred(String inErrorMessage) {
                Log.d("someUIErrorOccurred",""+inErrorMessage);
            }

            @Override
            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                Log.d("onErrorLoadingWebPage",""+iniErrorCode+" "+inErrorMessage+" "+inFailingUrl);
            }

            @Override
            public void onBackPressedCancelTransaction() {
                Log.d("onBackPressed","onBackPressedCancelTransaction");
            }

            @Override
            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                Log.d("onTransactionCancel",""+inErrorMessage+" "+inResponse);
            }
        });
    }
    public void chk() {
       /*  String merchantMid = "OKkAtf76247387509530";
        // Key in your staging and production MID available in your dashboard
        String merchantKey = "A@ohmtoKuJsBt4ee";
        // Key in your staging and production MID available in your dashboard
        String orderId = "order1";
        String channelId = "WAP";
        String custId = "cust123";
        String mobileNo = "7777777777";
        String email = "username@emailprovider.com";
        String txnAmount = "100.12";
        String website = "WEBSTAGING";
        // This is the staging value. Production value is available in your dashboard
        String industryTypeId = "Retail";
        // This is the staging value. Production value is available in your dashboard
        String callbackUrl = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=order1";


        TreeMap<String, String> paytmParams = new TreeMap<String, String>();
        paytmParams.put("MID",merchantMid);
        paytmParams.put("ORDER_ID",orderId);
        paytmParams.put("CHANNEL_ID",channelId);
        paytmParams.put("CUST_ID",custId);
        paytmParams.put("MOBILE_NO",mobileNo);
        paytmParams.put("EMAIL",email);
        paytmParams.put("TXN_AMOUNT",txnAmount);
        paytmParams.put("WEBSITE",website);
        paytmParams.put("INDUSTRY_TYPE_ID",industryTypeId);
        paytmParams.put("CALLBACK_URL", callbackUrl);
        //String paytmChecksum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(merchantKey, paytmParams);
        */
    }
    /*Staging: https://securegw-stage.paytm.in/theia/processTransaction
    Production: https://securegw.paytm.in/theia/processTransaction*/

    //call api async Task
    String orderid="txt222";
    public class sendUserDetailTOServerdd extends AsyncTask<ArrayList<String>, Void, String>{

      //  private ProgressDialog dialog = new ProgressDialog(LoginActivity.this);

        //private String orderId , mid, custid, amt;
        String url = "http://http://bhaijan.in/paytm/index.php";

        //String varifyurl = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=order111";
        String varifyurl = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=1";//+orderid;
        //String varifyurl = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
        String CHECKSUMHASH ="";

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
         //   this.dialog.setMessage("Please wait");
           // this.dialog.show();
        }

        @Override
        protected String doInBackground(ArrayList<String>... arrayLists) {
            JSONParserp jsonParser = new JSONParserp(LoginActivity.this);
            String param = "MID="+"rxazcv89315285244163"+
                    "&ORDER_ID="+"1"+
                    "&CUST_ID="+"1"+
                    "&INDUSTRY_TYPE_ID=Retail"+
                    "&CHANNEL_ID=WAP"+
                    "&TXN_AMOUNT=v"+
                    "&WEBSTAGING=http://bhaijan.in"+//+WEBSTAGING
                    "&CALLBACK_URL="+varifyurl;

            JSONObject jsonObject = jsonParser.makeHttpRequest(url,"POST",param);
            //here is get checksum with order id and status
            Log.d("Checksumresult",jsonObject.toString());
            if(jsonObject != null)
            {
                Log.d("checksum_result",jsonObject.toString());
                try
                {
                    CHECKSUMHASH = jsonObject.has("CHECKSUMHASH")?jsonObject.getString("CHECKSUMHASH"):"";
                }catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }


            return CHECKSUMHASH;
        }

        @Override
        protected void onPostExecute(String result) {
            //super.onPostExecute(result);
           /* if(dialog.isShowing())
            {
                dialog.dismiss();
            }*/
            //PaytmPGService Service = PaytmPGService.getStagingService();
            PaytmPGService Service = PaytmPGService.getProductionService();
            HashMap<String, String> paramMap = new HashMap<String,String>();
          /*  paramMap.put( "MID","OKkAtf76247387509530");
            //Key in your staging and production MID available in your dashboard
            paramMap.put( "ORDER_ID","order111");
            paramMap.put( "CUST_ID","cust111");
            // This is the staging value. Production value is available in your dashboard
            paramMap.put( "INDUSTRY_TYPE_ID","Retail");
            //paramMap.put( "MOBILE_NO" , "9867777860");
            //paramMap.put( "EMAIL" , "username@emailprovider.com");
            paramMap.put( "CHANNEL_ID","WAP");
            paramMap.put( "TXN_AMOUNT","100.11");
            paramMap.put( "WEBSITE","APPSTAGING");//WEBSTAGING
            // This is the staging value. Production value is available in your dashboard
            paramMap.put( "CALLBACK_URL",varifyurl);//"https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=order111"*/
            paramMap.put("MID","rxazcv89315285244163");
            paramMap.put("ORDER_ID","1");
            paramMap.put("CUST_ID","1");
            paramMap.put("INDUSTRY_TYPE_ID","Retail");
            paramMap.put("CHANNEL_ID","WAP");
            paramMap.put("TXN_AMOUNT","11.2");
            paramMap.put("WEBSITE","http://bhaijan.in");
            paramMap.put("CALLBACK_URL",varifyurl);
            paramMap.put( "CHECKSUMHASH" , CHECKSUMHASH);

            PaytmOrder Order = new PaytmOrder(paramMap);
            Service.initialize(Order,null);
            Service.startPaymentTransaction(LoginActivity.this, true, true, new PaytmPaymentTransactionCallback() {
                @Override
                public void onTransactionResponse(Bundle inResponse) {
                    Log.e("onTransactionResponse",""+inResponse);
                }

                @Override
                public void networkNotAvailable() {
                    Log.d("networkNotAvailable","networkNotAvailable");
                }

                @Override
                public void clientAuthenticationFailed(String inErrorMessage) {
                    Log.d("clientAuthFailed",""+inErrorMessage);
                }

                @Override
                public void someUIErrorOccurred(String inErrorMessage) {
                    Log.d("someUIErrorOccurred",""+inErrorMessage);
                }

                @Override
                public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                    Log.d("onErrorLoadingWebPage",""+inErrorMessage+" "+inErrorMessage+" "+inFailingUrl);
                }

                @Override
                public void onBackPressedCancelTransaction() {
                    Log.d("onBackPresse","onBackPressedCancelTransaction");
                }

                @Override
                public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                    Log.d("someUIErrorOccurred",""+inErrorMessage+" "+inResponse);
                }
            });
        }
    }


    //New Test Paytm
    PaytmPGService Service;
    HashMap<String, String> paramMap;
    JSONObject jsoNdata;
    PaytmOrder Order;
    String url = "http://bhaijan.in/paytm/index.php?merchantMid=rxazcv89315285244163&merchantKey=gKpu7IKaLSbkchFS&orderId=308&custId=1&mobileNo=9999999999&email=test@gmail.com&txnAmount=10";
    //String url = "http://mafatlaldarshan.com/checksum/generateChecksum.php";
    //String url1="http://mafatlaldarshan.com/checksum/verifyChecksum.php";

    public void onStartTransaction() {

        Service = PaytmPGService.getStagingService();

        paramMap = new HashMap<String, String>();

        //these are mandatory parameters
        paramMap.put("MID","rxazcv89315285244163");
        paramMap.put("ORDER_ID","308");
        paramMap.put("CUST_ID","1");
        paramMap.put("INDUSTRY_TYPE_ID","Retail");
        paramMap.put("CHANNEL_ID","WAP");
        paramMap.put("TXN_AMOUNT","10");
        paramMap.put("WEBSITE","WEBSTAGING");
        paramMap.put("CALLBACK_URL","https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=308");
        paramMap.put("MOBILE_NO","9999999999");
        paramMap.put("EMAIL","test@gmail.com");


        //paramMap.put("CALLBACK_URL","https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=txt22");
       // paramMap.put("CALLBACK_URL","http://bhaijan.in/bhaijan_paytm/verifyChecksum.php");
        // https://securegw.paytm.in/theia/processTransaction
        //http://bhaijan.in/bhaijan_paytm/verifyCheckSum.php

        jsoNdata = new JSONObject();
        try{
         /* //  jsoNdata.put("MID", "OKkAtf76247387509530");
            jsoNdata.put("ORDER_ID", "txt22");
            jsoNdata.put("CUST_ID", "cust12");
           // jsoNdata.put("INDUSTRY_TYPE_ID", "Retail");
           // jsoNdata.put("CHANNEL_ID","WAP");
            jsoNdata.put("TXN_AMOUNT","11.0");
          //  jsoNdata.put("WEBSITE","APPSTAGING");*/
         /*   jsoNdata.put("MID","rxazcv89315285244163");
            jsoNdata.put("ORDER_ID","1");
            jsoNdata.put("CUST_ID","1");
            jsoNdata.put("INDUSTRY_TYPE_ID","Retail");
            jsoNdata.put("CHANNEL_ID","WAB");
            jsoNdata.put("TXN_AMOUNT","11.2");
            jsoNdata.put("WEBSITE","WEBSTAGING");
            jsoNdata.put("CALLBACK_URL","https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=1");*/
            //https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=1
           // jsoNdata.put("CALLBACK_URL","http://bhaijan.in/bhaijan_paytm/verifyChecksum.php");
         /*   jsoNdata.put("merchantKey","gKpu7IKaLSbkchFS");
            jsoNdata.put("mobileNo","9999999999");
            jsoNdata.put("email","test@gmail.com");
            jsoNdata.put("txnAmount","10");*/
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex.getMessage(),Toast.LENGTH_LONG);
        }
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET,url, jsoNdata, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String checksum= jsonObject.getString("CHECKSUMHASH");
                    Log.e("CHECKSUMHASH",checksum);

                    paramMap.put("CHECKSUMHASH",checksum);

                    Order = new PaytmOrder(paramMap);

                    Service.initialize(Order, null);

                    Service.startPaymentTransaction(LoginActivity.this, true, true,
                            new PaytmPaymentTransactionCallback() {
                                @Override
                                public void someUIErrorOccurred(String inErrorMessage) {
                                    Log.d("someUIErrorOccurred", inErrorMessage);
                                }

                                @Override
                                public void onTransactionResponse(Bundle inResponse) {
                                    Log.e("LOG", "Payment Transaction is successful " + inResponse);
                                    Toast.makeText(getApplicationContext(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();
                                     // response.setText(inResponse.toString());
                                }

                                @Override
                                public void networkNotAvailable() { // If network is not
                                    Toast.makeText(getApplicationContext(),"networknotAvailable",Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void clientAuthenticationFailed(String inErrorMessage) {

                                }

                                @Override
                                public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {

                                }

                                // had to be added: NOTE
                                @Override
                                public void onBackPressedCancelTransaction() {
                                    Toast.makeText(LoginActivity.this,"Back pressed. Transaction cancelled",Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                                    Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                                    Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        AppController.getInstance().addToRequestQueue(objectRequest);
    }

}