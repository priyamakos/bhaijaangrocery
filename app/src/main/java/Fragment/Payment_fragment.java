package Fragment;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjit.bhaijan.LoginActivity;
import com.adjit.bhaijan.NetworkConnectivity.NetworkConnection;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import Config.BaseURL;
import Config.SharedPref;
import com.adjit.bhaijan.AppController;
import com.adjit.bhaijan.MainActivity;

import com.adjit.bhaijan.NetworkConnectivity.NetworkError;
/*import com.adjit.bhaijan.PaymentGatWay;
import com.adjit.bhaijan.Paytm;*/
import com.adjit.bhaijan.R;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import okio.Utf8;
import util.ConnectivityReceiver;
import util.CustomVolleyJsonRequest;
import util.DatabaseHandler;
import util.Session_management;

import static com.android.volley.VolleyLog.TAG;


public class Payment_fragment extends Fragment {
    RelativeLayout confirm;
    private DatabaseHandler db_cart;
    private Session_management sessionManagement;
    TextView payble_ammount, my_wallet_ammount;
    private String getlocation_id = "";
    private String getstore_id = "";
    private String gettime = "";
    private String getdate = "";
    private String getuser_id = "";
    private Double rewards;
    RadioButton rb_Store, rb_Cod, rb_card, rb_Netbanking, rb_paytm;
    CheckBox checkBox_Wallet, checkBox_coupon;
    EditText et_Coupon;
    String getvalue;
    String text;
    String cp;
    String total_amount;
    RadioGroup radioGroup;
    String Prefrence_TotalAmmount;
    String getwallet;
    LinearLayout Promo_code_layout;
    TextView Apply_Coupon_Code;
    String msg;
    String orderid;
    String mobile_num="",emailid="";
    String name;
    String sale_id;
    PaytmPGService Service;
    HashMap<String, String> paramMap;
    JSONObject jsoNdata;
    PaytmOrder Order;
 /*   String url = "http://bhaijan.in/paytm/index.php?merchantMid=rxazcv89315285244163&merchantKey=gKpu7IKaLSbkchFS&orderId="
            +order_id+"&custId="+getuser_id+"&mobileNo="+mobile_num+
            "&email="+emailid+"" +
            "&txnAmount="+total_amount;*/


    public Payment_fragment() {

    }

    public static Payment_fragment newInstance(String param1, String param2) {
        Payment_fragment fragment = new Payment_fragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_payment_method, container, false);
        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.payment));

        Prefrence_TotalAmmount = SharedPref.getString(getActivity(), BaseURL.TOTAL_AMOUNT);


        sessionManagement = new Session_management(getActivity());
        mobile_num=sessionManagement.getUserDetails().get(BaseURL.KEY_MOBILE);
        name = sessionManagement.getUserDetails().get(BaseURL.KEY_NAME);
        emailid=sessionManagement.getUserDetails().get(BaseURL.KEY_EMAIL);
        getuser_id = sessionManagement.getUserDetails().get(BaseURL.KEY_ID);

        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) group.findViewById(checkedId);
                getvalue = radioButton.getText().toString();
                Log.e("valueradiobtn",getvalue);


            }
        });

        Random r = new Random(System.currentTimeMillis());
        orderid = "ORDER" + (1 + r.nextInt(2)) * 10000
                + r.nextInt(10000);
        Log.e("orderId",orderid);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "Font/Bold.otf");
       // checkBox_Wallet = (CheckBox) view.findViewById(R.id.use_wallet);
      //  checkBox_Wallet.setTypeface(font);
        //rb_Store = (RadioButton) view.findViewById(R.id.use_store_pickup);
     //   rb_Store.setTypeface(font);
        rb_Cod = (RadioButton) view.findViewById(R.id.use_COD);
        rb_Cod.setTypeface(font);
      //  rb_card = (RadioButton) view.findViewById(R.id.use_card);
      //  rb_card.setTypeface(font);
       // rb_Netbanking = (RadioButton) view.findViewById(R.id.use_netbanking);
       // rb_Netbanking.setTypeface(font);
        rb_paytm = (RadioButton) view.findViewById(R.id.use_wallet_ammount);
        rb_paytm.setTypeface(font);
       // checkBox_coupon = (CheckBox) view.findViewById(R.id.use_coupon);
        //checkBox_coupon.setTypeface(font);
        et_Coupon = (EditText) view.findViewById(R.id.et_coupon_code);
        cp = et_Coupon.getText().toString();
        Promo_code_layout = (LinearLayout) view.findViewById(R.id.prommocode_layout);
        Apply_Coupon_Code = (TextView) view.findViewById(R.id.apply_coupoun_code);
        sessionManagement = new Session_management(getActivity());

        //Show  Wallet
        getwallet = SharedPref.getString(getActivity(), BaseURL.KEY_WALLET_Ammount);
      //  my_wallet_ammount = (TextView) view.findViewById(R.id.user_wallet);
      //  my_wallet_ammount.setText(getActivity().getString(R.string.currency) + getwallet);
        db_cart = new DatabaseHandler(getActivity());
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Fragment fm = new Home_fragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                            .addToBackStack(null).commit();
                    return true;
                }
                return false;
            }
        });


        total_amount = getArguments().getString("total");
        getdate = getArguments().getString("getdate");
        gettime = getArguments().getString("gettime");
        getlocation_id = getArguments().getString("getlocationid");
        getstore_id = getArguments().getString("getstoreid");
        Log.e("stroreid",getArguments().getString("getstoreid"));
        payble_ammount = (TextView) view.findViewById(R.id.payable_ammount);
        payble_ammount.setText(getActivity().getString(R.string.currency) + total_amount);


     /*   checkBox_Wallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    Use_Wallet_Ammont();
                    if (rb_card.isChecked() || rb_Netbanking.isChecked() || rb_paytm.isChecked() || checkBox_coupon.isChecked()) {
                        rb_card.setChecked(false);
                        rb_Netbanking.setChecked(false);
                        rb_paytm.setChecked(false);
                        checkBox_coupon.setChecked(false);
                    }
                } else {
                    if (payble_ammount != null) {
                        rb_Cod.setText("Cash On Delivery");
                        rb_card.setClickable(true);
                        rb_card.setTextColor(getResources().getColor(R.color.dark_black));
                        rb_Netbanking.setClickable(true);
                        rb_Netbanking.setTextColor(getResources().getColor(R.color.dark_black));
                        rb_paytm.setClickable(true);
                        rb_paytm.setTextColor(getResources().getColor(R.color.dark_black));
                        checkBox_coupon.setClickable(true);
                        checkBox_coupon.setTextColor(getResources().getColor(R.color.dark_black));
                    }
                    final String Ammount = SharedPref.getString(getActivity(), BaseURL.TOTAL_AMOUNT);
                    final String WAmmount = SharedPref.getString(getActivity(), BaseURL.KEY_WALLET_Ammount);
                    my_wallet_ammount.setText(getActivity().getResources().getString(R.string.currency) + WAmmount);
                    payble_ammount.setText(getResources().getString(R.string.currency) + Ammount);
                }
            }
        });*/

       /* checkBox_coupon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Promo_code_layout.setVisibility(View.VISIBLE);

                } else {
                    Promo_code_layout.setVisibility(View.GONE);

                }
            }
        });*/


        confirm = (RelativeLayout) view.findViewById(R.id.confirm_order);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    checked();

                } else {
                    ((MainActivity) getActivity()).onNetworkConnectionChanged(false);
                }
            }
        });


        return view;
    }

    private void attemptOrder() {
        ArrayList<HashMap<String, String>> items = db_cart.getCartAll();
        if(db_cart.getColumnRewards().equals("")){
            rewards=0.0;
        }else {
            rewards = Double.parseDouble(db_cart.getColumnRewards());
        }

        if (items.size() > 0) {
            JSONArray passArray = new JSONArray();
            for (int i = 0; i < items.size(); i++) {
                HashMap<String, String> map = items.get(i);
                JSONObject jObjP = new JSONObject();
                try {
                    jObjP.put("product_id", map.get("product_id"));
                    jObjP.put("qty", map.get("qty"));
                    jObjP.put("unit_value", map.get("unit_value"));
                    jObjP.put("unit", map.get("unit"));
                    jObjP.put("price", map.get("price"));
                    jObjP.put("rewards", 0);
                    passArray.put(jObjP);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            if (ConnectivityReceiver.isConnected()) {

                Log.e(TAG, "from:" + gettime + "\ndate:" + getdate +
                        "\n" + "\nuser_id:" + getuser_id + "\n" + getlocation_id + getstore_id + "\ndata:" + passArray.toString());

                makeAddOrderRequest(getdate, gettime, getuser_id, getlocation_id, getstore_id, passArray);
            }
        }
    }

    private void makeAddOrderRequest(String date, String gettime, String userid, String
            location, String store_id, JSONArray passArray) {
        String tag_json_obj = "json_add_order_req";
        Map<String, String> params = new HashMap<String, String>();
        params.put("date", date);
        params.put("time", gettime);
        params.put("user_id", userid);
        params.put("location", location);
        params.put("store_id", store_id);
        params.put("payment_method", getvalue);
        params.put("data", passArray.toString());
        Log.e("valuesorder",date +""+ gettime+" "+userid+" "+location+" "+" "+store_id+" "+getvalue );
        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.ADD_ORDER_URL, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                Log.e("resposeorder", String.valueOf(response));

                try {
                    Boolean status = response.getBoolean("responce");
                    if (status) {

                        msg = response.getString("data");
                        sale_id=response.getString("order_id");
                        Log.e("sale_id",sale_id);
                      if(getvalue.equals("Paytm")){
                            //onStartTransaction(response.getString("order_id"));
                            onStartTransaction(orderid);
                        }else {
                            sms_api();
                            sms_api_customer();
                            db_cart.clearCart();
                            Bundle args = new Bundle();
                            Fragment fm = new Thanks_fragment();
                            args.putString("msg", msg);
                            fm.setArguments(args);
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.contentPanel, fm)
                                    .addToBackStack(null).commit();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void sms_api_customer() {

        String ordemsg="Your Order is successfully placed, Your orderID is  "+ sale_id+" For any help call us at 8906005050 ";
        String sms_url= null;
        try {
            sms_url = "http://sms.adjinfotech.com/api/sendmsg.php?user=bhaijaan&pass=bhaijaan&sender=BHIJAN&phone="+mobile_num+
                    "&text=" + URLEncoder.encode(ordemsg,"UTF-8").toString()+
                    "&priority=ndnd&stype=normal";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.e("smsurl",sms_url);
        if (NetworkConnection.connectionChecking(getActivity())) {
            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, sms_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("smsres",response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            rq.add(stringRequest);
        }
    }

    private void sms_api() {

        String ordemsg="You have received new order from "+name+" with order id "+sale_id+" mobile number "+mobile_num;
        String sms_url= null;
        try {
            sms_url = "http://sms.adjinfotech.com/api/sendmsg.php?user=bhaijaan&pass=bhaijaan&sender=BHIJAN&phone=8906005050,9867777860"+
                    "&text=" + URLEncoder.encode(ordemsg,"UTF-8").toString()+
                    "&priority=ndnd&stype=normal";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.e("smsurl",sms_url);
        if (NetworkConnection.connectionChecking(getActivity())) {
            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, sms_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("smsres",response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            rq.add(stringRequest);
        }
    }



    private void Use_Wallet_Ammont() {
        final String Wallet_Ammount = SharedPref.getString(getActivity(), BaseURL.KEY_WALLET_Ammount);
        final String Ammount = SharedPref.getString(getActivity(), BaseURL.TOTAL_AMOUNT);
        if (NetworkConnection.connectionChecking(getActivity())) {
            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest postReq = new StringRequest(Request.Method.POST, "http://neerajbisht.com/grocery_test/index.php/api/wallet_on_checkout",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("eclipse", "Response=" + response);
                            try {
                                JSONObject object = new JSONObject(response);
                                JSONArray Jarray = object.getJSONArray("final_amount");
                                for (int i = 0; i < Jarray.length(); i++) {
                                    JSONObject json_data = Jarray.getJSONObject(i);
                                    String Wallet_amount = json_data.getString("wallet");
                                    total_amount = json_data.getString("total");
                                    if (total_amount.equals("0")) {
                                        rb_Cod.setText("Home Delivery");
                                        getvalue = rb_Cod.getText().toString();
                                        rb_card.setClickable(false);
                                        rb_card.setTextColor(getResources().getColor(R.color.gray));
                                        rb_Netbanking.setClickable(false);
                                        rb_Netbanking.setTextColor(getResources().getColor(R.color.gray));
                                        rb_paytm.setClickable(false);
                                        rb_paytm.setTextColor(getResources().getColor(R.color.gray));
                                        checkBox_coupon.setClickable(false);
                                        checkBox_coupon.setTextColor(getResources().getColor(R.color.gray));
                                    } else {
                                        if (total_amount != null) {
                                            rb_Cod.setText("Cash On Delivery");
                                            rb_card.setClickable(true);
                                            rb_card.setTextColor(getResources().getColor(R.color.dark_black));
                                            rb_Netbanking.setClickable(true);
                                            rb_Netbanking.setTextColor(getResources().getColor(R.color.dark_black));
                                            rb_paytm.setClickable(true);
                                            rb_paytm.setTextColor(getResources().getColor(R.color.dark_black));
                                            checkBox_coupon.setClickable(true);
                                            checkBox_coupon.setTextColor(getResources().getColor(R.color.dark_black));
                                        }
                                    }
                                    payble_ammount.setText(getResources().getString(R.string.currency) + total_amount);
                                   // my_wallet_ammount.setText(getResources().getString(R.string.currency) + Wallet_amount);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("Error [" + error + "]");
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("wallet_amount", Wallet_Ammount);
                    params.put("total_amount", Ammount);
                    return params;
                }
            };
            rq.add(postReq);
        } else {
            Intent intent = new Intent(getActivity(), NetworkError.class);
            startActivity(intent);
        }
    }

    private void checked() {
       /* if (checkBox_Wallet.isChecked()) {
            if (rb_Store.isChecked() || rb_Cod.isChecked()) {
                attemptOrder();
            } else {
                Toast.makeText(getActivity(), "Please Select One", Toast.LENGTH_SHORT).show();
            }

        }*/
     /*   if (rb_Store.isChecked()) {
            attemptOrder();
        }*/
        if (rb_Cod.isChecked()) {
            attemptOrder();
        }
      /*  if (rb_card.isChecked()) {
            Intent myIntent = new Intent(getActivity(), PaymentGatWay.class);
            if (checkBox_Wallet.isChecked()) {
                myIntent.putExtra("total", total_amount);
            } else {
                myIntent.putExtra("total", Prefrence_TotalAmmount);
                myIntent.putExtra("getdate", getdate);
                myIntent.putExtra("gettime", gettime);
                myIntent.putExtra("getlocationid", getlocation_id);
                myIntent.putExtra("getstoreid", getstore_id);
                myIntent.putExtra("getpaymentmethod", getvalue);
            }
            getActivity().startActivity(myIntent);
        }*/
       /* if (rb_Netbanking.isChecked()) {
            Intent myIntent1 = new Intent(getActivity(), PaymentGatWay.class);
            if (checkBox_Wallet.isChecked()) {
                myIntent1.putExtra("total", total_amount);

            } else {
                myIntent1.putExtra("total", Prefrence_TotalAmmount);
                myIntent1.putExtra("getdate", getdate);
                myIntent1.putExtra("gettime", gettime);
                myIntent1.putExtra("getlocationid", getlocation_id);
                myIntent1.putExtra("getstoreid", getstore_id);
                myIntent1.putExtra("getpaymentmethod", getvalue);
            }
            getActivity().startActivity(myIntent1);
        }*/
        if (rb_paytm.isChecked()) {
            attemptOrder();
           // onStartTransaction();
        }
         /*   Intent myIntent1 = new Intent(getActivity(), Paytm.class);
            if (checkBox_Wallet.isChecked()) {
                myIntent1.putExtra("total", total_amount);

            } else {
                myIntent1.putExtra("total", Prefrence_TotalAmmount);
                myIntent1.putExtra("getdate", getdate);
                myIntent1.putExtra("gettime", gettime);
                myIntent1.putExtra("getlocationid", getlocation_id);
                myIntent1.putExtra("getstoreid", getstore_id);
                myIntent1.putExtra("getpaymentmethod", getvalue);
            }
            getActivity().startActivity(myIntent1);

        }*/
/*        if (checkBox_coupon.isChecked()) {
                Toast.makeText(getActivity(), "Coupon Section InProgress", Toast.LENGTH_SHORT).show();


        }*/


    }

    //paytem payment getway
    public void paytm_getway()
    {
        PaytmPGService Service = PaytmPGService.getStagingService();
        //PaytmPGService Service = PaytmPGService.getProductionService();

        HashMap<String, String> paramMap = new HashMap<String,String>();
        paramMap.put( "MID" , "rxazcv89315285244163");
        // Key in your staging and production MID available in your dashboard
        paramMap.put( "ORDER_ID" , "order1");
        paramMap.put( "CUST_ID" , "cust123");
        paramMap.put( "MOBILE_NO" , "7777777777");
        paramMap.put( "EMAIL" , "username@emailprovider.com");
        paramMap.put( "CHANNEL_ID" , "WAP");
        paramMap.put( "TXN_AMOUNT" , "100.12");
        paramMap.put( "WEBSITE" , "WEBSTAGING");
        // This is the staging value. Production value is available in your dashboard
        paramMap.put( "INDUSTRY_TYPE_ID" , "Retail");
        // This is the staging value. Production value is available in your dashboard
        paramMap.put( "CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=order1");
        paramMap.put( "CHECKSUMHASH" , "w2QDRMgp1234567JEAPCIOmNgQvsi+BhpqijfM9KvFfRiPmGSt3Ddzw+oTaGCLneJwxFFq5mqTMwJXdQE2EzK4px2xruDqKZjHupz9yXev4=");
        PaytmOrder Order = new PaytmOrder(paramMap);
        Service.initialize(Order,null);
        Service.startPaymentTransaction(getActivity(), true, true, new PaytmPaymentTransactionCallback() {
            @Override
            public void onTransactionResponse(Bundle inResponse) {

            }

            @Override
            public void networkNotAvailable() {

            }

            @Override
            public void clientAuthenticationFailed(String inErrorMessage) {

            }

            @Override
            public void someUIErrorOccurred(String inErrorMessage) {

            }

            @Override
            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {

            }

            @Override
            public void onBackPressedCancelTransaction() {

            }

            @Override
            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {

            }
        });
    }

    public void onStartTransaction(String orderid) {
//MID rxazcv89315285244163    MKEY gKpu7IKaLSbkchFS
        String url = "http://bhaijan.in/paytm/index.php?merchantMid=OKkAtf76247387509530&merchantKey=A@ohmtoKuJsBt4ee&orderId="+orderid
                +"&custId="+getuser_id
                +"&mobileNo="+mobile_num+
                "&email="+emailid+
                "&txnAmount="+total_amount;
        Log.e("urlpaytm",url);
        //Service = PaytmPGService.getStagingService();
        Service = PaytmPGService.getProductionService();

        paramMap = new HashMap<String, String>();

        //these are mandatory parameters
        paramMap.put("MID","OKkAtf76247387509530");
        paramMap.put("ORDER_ID",orderid);
        paramMap.put("CUST_ID",getuser_id);
        paramMap.put("INDUSTRY_TYPE_ID","Retail");
        paramMap.put("CHANNEL_ID","WAP");
        paramMap.put("TXN_AMOUNT",total_amount);
        paramMap.put("WEBSITE","WEBSTAGING");
        paramMap.put("CALLBACK_URL","https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderid);
        paramMap.put("MOBILE_NO",mobile_num);
        paramMap.put("EMAIL",emailid);

        //for staging  https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=

        //paramMap.put("CALLBACK_URL","https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=txt22");
        // paramMap.put("CALLBACK_URL","http://bhaijan.in/bhaijan_paytm/verifyChecksum.php");
        // https://securegw.paytm.in/theia/processTransaction
        //http://bhaijan.in/bhaijan_paytm/verifyCheckSum.php

        jsoNdata = new JSONObject();
        try{
         /* //  jsoNdata.put("MID", "OKkAtf76247387509530");
            jsoNdata.put("ORDER_ID", "txt22");
            jsoNdata.put("CUST_ID", "cust12");
           // jsoNdata.put("INDUSTRY_TYPE_ID", "Retail");
           // jsoNdata.put("CHANNEL_ID","WAP");
            jsoNdata.put("TXN_AMOUNT","11.0");
          //  jsoNdata.put("WEBSITE","APPSTAGING");*/
         /*   jsoNdata.put("MID","rxazcv89315285244163");
            jsoNdata.put("ORDER_ID","1");
            jsoNdata.put("CUST_ID","1");
            jsoNdata.put("INDUSTRY_TYPE_ID","Retail");
            jsoNdata.put("CHANNEL_ID","WAB");
            jsoNdata.put("TXN_AMOUNT","11.2");
            jsoNdata.put("WEBSITE","WEBSTAGING");
            jsoNdata.put("CALLBACK_URL","https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=1");*/
            //https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=1
            // jsoNdata.put("CALLBACK_URL","http://bhaijan.in/bhaijan_paytm/verifyChecksum.php");
         /*   jsoNdata.put("merchantKey","gKpu7IKaLSbkchFS");
            jsoNdata.put("mobileNo","9999999999");
            jsoNdata.put("email","test@gmail.com");
            jsoNdata.put("txnAmount","10");*/
        }catch (Exception ex){
            Toast.makeText(getActivity(),ex.getMessage(),Toast.LENGTH_LONG);
        }
        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET,url, jsoNdata, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                try {
                    String checksum= jsonObject.getString("CHECKSUMHASH");
                    Log.e("CHECKSUMHASH",checksum);

                    paramMap.put("CHECKSUMHASH",checksum);

                    Order = new PaytmOrder(paramMap);

                    Service.initialize(Order, null);

                    Service.startPaymentTransaction(getActivity(), true, true,
                            new PaytmPaymentTransactionCallback() {
                                @Override
                                public void someUIErrorOccurred(String inErrorMessage) {
                                    Log.d("someUIErrorOccurred", inErrorMessage);
                                }

                                @Override
                                public void onTransactionResponse(Bundle inResponse)
                                {
                                    Log.e("LOG", "Payment Transaction is successful " + inResponse);
                                    Log.e("txnid",inResponse.getString("STATUS"));

                                    if(inResponse.getString("STATUS").equals("TXN_SUCCESS"))
                                    {
                                        String TXNID = inResponse.getString("TXNID");
                                        String ORDERID = inResponse.getString("ORDERID");
                                        edit_orderpayemnt_api(TXNID,ORDERID);
                                        //edit_orderpayemnt_api(inResponse.getString("TXNID"),inResponse.getString("ORDERID"));
                                        sms_api();
                                        sms_api_customer();
                                        db_cart.clearCart();
                                        Bundle args = new Bundle();
                                        Fragment fm = new Thanks_fragment();
                                        args.putString("msg", msg);
                                        fm.setArguments(args);
                                        FragmentManager fragmentManager = getFragmentManager();
                                        fragmentManager.beginTransaction().replace(R.id.contentPanel, fm).addToBackStack(null).commit();

                                       // Toast.makeText(getActivity(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();
                                    }else {
                                        db_cart.clearCart();
                                        Intent intent=new Intent(getActivity(),MainActivity.class);
                                        startActivity(intent);
                                        Toast.makeText(getActivity(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();
                                    }

                                    // response.setText(inResponse.toString());

                                }

                                @Override
                                public void networkNotAvailable() { // If network is not
                                    Toast.makeText(getActivity(),"networknotAvailable",Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void clientAuthenticationFailed(String inErrorMessage) {

                                }

                                @Override
                                public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {

                                }

                                // had to be added: NOTE
                                @Override
                                public void onBackPressedCancelTransaction() {
                                    Toast.makeText(getActivity(),"Back pressed. Transaction cancelled",Toast.LENGTH_LONG).show();
                                    db_cart.clearCart();
                                    Intent intent=new Intent(getActivity(),MainActivity.class);
                                    startActivity(intent);
                                }

                                @Override
                                public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                                    Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                                    db_cart.clearCart();
                                    Toast.makeText(getActivity(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                                    Intent intent=new Intent(getActivity(),MainActivity.class);
                                    startActivity(intent);
                                }

                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        AppController.getInstance().addToRequestQueue(objectRequest);
    }

    private void edit_orderpayemnt_api(final String status, final String orderid)
    {
        String edit_orderpayemnt_url="http://bhaijan.in/groceryapp/index.php/api/edit_order_payment";

        if (NetworkConnection.connectionChecking(getActivity()))
        {
            RequestQueue rq = Volley.newRequestQueue(getActivity());
            StringRequest stringRequest = new StringRequest(Request.Method.POST, edit_orderpayemnt_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    Log.e("orderpaytm",response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map=new HashMap<String, String>();
                    Log.e("params",sale_id+" "+orderid+"  "+status);
                    map.put("sale_id",sale_id);
                    map.put("txn_id",status);
                    map.put("order_id ",orderid);
                    return map;
                }
            };
            rq.add(stringRequest);
        }
    }

}
